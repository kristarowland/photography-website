// script js Krista's Photography 

$(document).ready(function()
{
  $('.image').hover(
    function()
    {
      $(this).animate({
        opacity: 0.5,
      })
    },
    function()
    {
      $(this).animate({
        opacity: 1,
      })
    }
  );

  $("nav .link").click(function() {
    var attr = $(this).attr('data-target-tag');
    sort(attr);
  });

  $("[data-fancybox]").fancybox({
      buttons : [
        'fullScreen',
        'close'
      ]});
});

function sort(tag)
{
  $('.photo-container a').each(
    function()
    {
      var attr = $(this).attr('data-tags');

      if (attr)
      {
        var tags = attr.split(" ");

        if (tags.indexOf(tag) > -1)
        {
          $(this).show();
          return;
        }
      }

      $(this).hide();
    }
  )
}